//
//  ViewController.m
//  array
//
//  Created by Clicklabs 104 on 9/21/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *maths;
@property (weak, nonatomic) IBOutlet UILabel *english;
@property (weak, nonatomic) IBOutlet UILabel *science;
@property (weak, nonatomic) IBOutlet UILabel *marks;
@property (weak, nonatomic) IBOutlet UILabel *tmaths;

@property (weak, nonatomic) IBOutlet UITextField *tenglish;
@property (weak, nonatomic) IBOutlet UITextField *tscience;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textfield{
    NSLog(@"textFieldShouldEndEditing");
    textfield.backgroundColor = [UIColor yellowColor];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textfield{
    NSLog(@"textFieldDidEndEditing");
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing");
    textField.backgroundColor = [UIColor greenColor];
    if(textField == _tmaths)
    {
        _maths.textColor= [UIColor purpleColor];
    }
    
    else if(textField == _tscience){
        _science.textColor= [UIColor purpleColor];
    }
   
    else if(textField == _tenglish){
        _english.textColor= [UIColor purpleColor];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldDidBeginEditing");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
